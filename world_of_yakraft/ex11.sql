-- write a query that returns all the titles of the quests, and the name of the
-- creatures that starts them.

SELECT title, name
  FROM quest
    JOIN creature ON quest.creature_start = creature.id
    JOIN creature_template ON creature.gid = creature_template.id;
