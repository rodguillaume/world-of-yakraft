-- write a query that returns the names of the creatures that never appear in
-- the game (the template exists but is never “instantiated”)

SELECT name
  FROM creature_template
    WHERE creature_template.id NOT IN (
      SELECT distinct gid
        FROM creature
    );
