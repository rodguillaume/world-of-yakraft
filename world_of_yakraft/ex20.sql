-- write a request that returns the list of the names of the creatures the
-- character ‘Tilon’ potentially killed to get the items he owns in his
-- inventory.

SELECT creature_template.name as name
  FROM inventory
    JOIN character ON inventory.character_id = character.id
    JOIN loots ON loots.item_id = inventory.item_id
    JOIN creature_template ON creature_template.id = loots.creature_gid
      WHERE character.name LIKE 'Tilon';
