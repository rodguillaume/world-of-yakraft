-- The player Tilon won a contest during a special event. Write a request that
-- gives him the level 15.

UPDATE character
  SET level = 15
    WHERE name LIKE 'Tilon';

-- The player Kuro complained that his level was not updated despite the fact
-- that he gained enough experience. Write a request that increments his level
-- by one.

UPDATE character
  SET level = level + 1
    WHERE name LIKE 'Kuro';

-- You decided to change the way the max_health stat is computed for characters.
-- It is now (level+1)∗10 if the character is male, and level ∗ 10 if the
-- character is female. Write a request that updates this stat for all the
-- characters.

UPDATE character
  SET max_health =  CASE character.gender
                      WHEN 0 THEN (character.level + 1) * 10  -- male
                      ELSE (character.level) * 10             -- female
                    END;
