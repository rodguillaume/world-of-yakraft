-- write a query that returns the names of the characters ordered by how
-- advanced in the game they are, in ascending order. To decide between
-- characters with the same level, sort by experience. To decide between
-- characters with the same experience, sort by the amount of gold they own.

SELECT name
  FROM character
ORDER BY level, xp, gold;
