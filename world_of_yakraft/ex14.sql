-- write a query that returns the title of the quests that the character Kuro
-- completed

SELECT title
  FROM character_quests
    JOIN character ON character_quests.character_id = character.id
    JOIN quest ON character_quests.quest_id = quest.id
      WHERE character.name LIKE 'Kuro';
