-- write a query that returns the id of all the creatures (the instances, not
-- the templates) whose level is strictly superior to 10

SELECT creature.id
  FROM creature
    JOIN creature_template ON creature.gid = creature_template.id
      WHERE creature_template.level > 10;
