-- write a query that returns a list of the names of the different quests and
-- the characters that started it without completing it. (complete = 0)

SELECT title, name
  FROM character
    JOIN character_quests ON character.id = character_quests.character_id
    JOIN quest ON quest.id = character_quests.quest_id
      WHERE character_quests.complete = 0;
