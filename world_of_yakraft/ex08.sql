-- To please high level players, you decided to add some difficult creatures in
-- the game. Their name is ‘Wild YAKA’, they are level 99, their 3D model ID is
-- 1387, they give 1000 experience points and 1000 gold when killed, their
-- health is 8, their speed is 8 and their attack is 50.

INSERT INTO creature_template VALUES
  (DEFAULT, 'Wild YAKA', 99, 1387, 1000, 8, 8, 50, 1000);

-- Add three instances of the Young Wolf (id = 1) to spawn at positions:
-- (5, 6, 7)
-- (−3, −2, −1)
-- (42, 43, 44)

INSERT INTO creature VALUES
  (DEFAULT, 1, 5, 6, 7),
  (DEFAULT, 1, -3, -2, -1),
  (DEFAULT, 1, 42, 43, 44);
