-- write a query that displays all the characters name whose last connection was
-- that date January 10th 2017

SELECT name
  FROM character
    WHERE lastconn::date = '2017-01-10'::date;
