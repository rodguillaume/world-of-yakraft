-- write a query that removes all the characters that haven’t been connected for
-- more than a year (31556926 seconds). For the exercise purpose, assume that
-- you don’t need to remove the useless rows in the other tables.

DELETE FROM character
  WHERE lastconn < now() - interval '1 year';
