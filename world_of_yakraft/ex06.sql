-- write a query that returns the titles of the quests that are “worth it” for
-- your level 10 character. You realized that it’s worth it to do a quest when
-- the creature that starts the quest is the same as the one that ends it
-- (because you usually don’t have to travel a lot) or the gold reward is more
-- than 100. In any case, the level requirement must be at least 8, but not more
-- than 10 (because you can’t do the ones with a higher requirement).

SELECT title
  FROM quest
    WHERE (level_min BETWEEN 8 AND 10)
          AND (creature_start = creature_end OR gold > 100);
