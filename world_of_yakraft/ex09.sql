-- write a query that returns the name of the creature whose ID is 6 (the
-- creature ID, not the template)

SELECT name
  FROM creature
    JOIN creature_template
      ON creature.gid = creature_template.id
    WHERE creature.id = 6;
