-- write a query that returns the title of all the quests, sorted by the length
-- of their titles, in ascending order.

SELECT title
  FROM (
    SELECT title, length(title) as length
      FROM quest
    ORDER BY length
  ) as title_by_length;
