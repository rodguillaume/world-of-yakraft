-- To understand which creatures are friendly and which ones aren’t, we want to
-- have a way to tell what their “faction” is.

-- Create a faction table which contains:
-- • A field id, primary key, type SERIAL.
-- • A field color, integer containing the color of the faction.
-- • A field name, string of at most 64 characters, containing the name of the
-- faction.

DROP TABLE IF EXISTS faction CASCADE;

CREATE TABLE faction (
  id    SERIAL,
  color INT         NOT NULL,
  name  varchar(64) NOT NULL,

  PRIMARY KEY (id)
);

-- Insert inside your table a faction with the id 0, the color 0x00FF00 and the
-- name Enlightened.

INSERT INTO faction VALUES
  (0, x'00FF00'::INT, 'Enlightened');

-- Then, add a column faction_id in the creature table, which is a foreign key
-- to the faction table.

ALTER TABLE creature
  ADD faction_id INT NOT NULL DEFAULT 0;

ALTER TABLE creature
  ADD CONSTRAINT faction_id_fk
  FOREIGN KEY (faction_id)
  REFERENCES faction(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE;
