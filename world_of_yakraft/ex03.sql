-- write a query that returns the names of the top 5 characters, ordered by
-- experience in descending order.

SELECT name
  FROM character
ORDER BY xp DESC
LIMIT 5;
