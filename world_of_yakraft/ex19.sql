-- You will need to create 3 tables in ex19.sql.

-- item contains the fields:
-- • id, a primary key
-- • name, a string which contains at most 64 characters, for the name of the
-- item
-- • rarity, an integer for the rarity of the item
-- • icon_id, an integer that contains the icon ID of the item

DROP TABLE IF EXISTS item CASCADE;

CREATE TABLE item (
  id      SERIAL,
  name    VARCHAR(64) NOT NULL,
  rarity  INT         NOT NULL,
  icon_id INT         NOT NULL,

  PRIMARY KEY (id)
);

-- inventory is used to represent a many-to-many relationship between the items
-- and the characters. A row means that the specified character owns the
-- specified item. The table contains:
-- • id, a primary key
-- • item_id, a foreign key to the item ID
-- • character_id, a foreign key to the character ID

DROP TABLE IF EXISTS inventory CASCADE;

CREATE TABLE inventory (
  id            SERIAL,
  item_id       INT NOT NULL DEFAULT 0,
  character_id  INT NOT NULL DEFAULT 0,

  PRIMARY KEY (id),
  FOREIGN KEY (item_id) REFERENCES item(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (character_id) REFERENCES character(id)
    ON UPDATE CASCADE ON DELETE CASCADE
);

-- loots is used to represent a many-to-many relationship between the items and
-- the creatures. A row means the specified creature can drop the specified
-- item. The table contains:
-- • id, a primary key
-- • item_id, a foreign key to the item ID
-- • creature_gid, a foreign key to the creature template ID.

DROP TABLE IF EXISTS loots CASCADE;

CREATE TABLE loots (
  id            SERIAL,
  item_id       INT NOT NULL DEFAULT 0,
  creature_gid  INT NOT NULL DEFAULT 0,

  PRIMARY KEY (id),
  FOREIGN KEY (item_id) REFERENCES item(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (creature_gid) REFERENCES creature_template(id)
    ON UPDATE CASCADE ON DELETE CASCADE
);
