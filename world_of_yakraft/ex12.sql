-- write a query that returns the name of the creatures that are the same at the
-- start and the end of a quest.

SELECT name
  FROM quest
    JOIN creature ON quest.creature_start = creature.id
    JOIN creature_template ON creature.gid = creature_template.id
      WHERE creature_start = creature_end;
