-- write a query that returns the names of all the dead characters of the server
-- (which are the characters whose health is equal to zero).

SELECT name
  FROM character
    WHERE health = 0;
